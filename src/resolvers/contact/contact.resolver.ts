import { Query, Resolver, ResolveProperty, Parent } from '@nestjs/graphql';
import { Contact } from './../../models/contact';
import { PrismaService } from 'src/services/prisma.service';
import { GqlAuthGuard } from '../../guards/gql-auth.guard';
// import { UserIdArgs } from 'src/models/args/userid-args';
import { UserEntity } from 'src/decorators/user.decorator';
import { User } from './../../models/user';
import { UseGuards } from '@nestjs/common';

@Resolver(of => Contact)
export class ContactResolver {
  constructor(private prisma: PrismaService) {}

  @UseGuards(GqlAuthGuard) // @UserEntity is only available if this authguard is in place!
  @Query(returns => [Contact])
  userContacts(@UserEntity() user: User) {
    return this.prisma.user.findOne({ where: { id: user.id } }).contacts();
  }

  @ResolveProperty('contactOf')
  async contactOf(@Parent() contact: Contact) {
    return this.prisma.contact.findOne({ where: { id: contact.id } });
  }

  @ResolveProperty('emails')
  emails(@Parent() contact: Contact) {
    return this.prisma.contact.findOne({ where: { id: contact.id } }).emails();
  }
  @ResolveProperty('phones')
  phones(@Parent() contact: Contact) {
    return this.prisma.contact.findOne({ where: { id: contact.id } }).phones();
  }
}
