import { ContactResolver } from './contact.resolver';
import { Module } from '@nestjs/common';
import { PrismaService } from '../../services/prisma.service';

@Module({
  providers: [ContactResolver, PrismaService]
})
export class ContactModule {}
