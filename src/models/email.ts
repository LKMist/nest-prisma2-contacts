import { Field, ObjectType } from 'type-graphql';
import { Model } from './model';
import { Contact } from './contact';

@ObjectType()
export class Email extends Model {
  @Field(type => String)
  email: string;

  @Field(type => Contact)
  emailOf: Contact;
}
