import { ObjectType, Field } from 'type-graphql';
import { Model } from './model';
import { Contact } from './contact';

@ObjectType()
export class Phone extends Model {
  @Field(type => String)
  number: string;

  @Field(type => Contact)
  phoneOf: Contact;
}
