import { Field, ObjectType } from 'type-graphql';
import { Model } from './model';
import { Email } from './email';
import { Phone } from './phone';
import { User } from './user';

@ObjectType()
export class Contact extends Model {
  @Field(type => String) // In a string we can do away with (type => String) but keeping it for consistancy.
  firstname: string;

  @Field(type => String)
  lastname: string;

  @Field(type => [Email])
  emails: Email[];

  @Field(type => [Phone])
  phones: Phone[];

  @Field(type => Boolean)
  published: boolean;

  @Field(type => User)
  contactOf: User;
}
